<?php


use App\Http\Controllers\MessageController;

Route::get('/', function () {
    return view('admin\home');
});

Auth::routes();

Route::resource('BaseCar', 'BaseCarController');
Route::resource('NewCar', 'NewCarController');

Route::get('msg', 'MessageController@index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', function(){
    return view('admin\home');
});