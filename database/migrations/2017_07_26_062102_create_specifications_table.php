<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('engine_size')->nullable();
            $table->integer('CC');
            $table->integer('torque')->nullable();
            $table->integer('horse_Power')->nullable();
            $table->integer('AirBags');
            $table->integer('seats')->nullable();
            $table->integer('year_of_Manufacturer')->nullable();
            $table->integer('baseCar_id')->unsigned();
            $table->string('gearBox');
            $table->string('bodyType')->nullable();
            $table->string('MultiMedia')->nullable();
            $table->boolean('power_steering');
            $table->boolean('power_windows');
            $table->boolean('ABS');
            $table->boolean('SunRoof');
            $table->foreign('baseCar_id')->references('id')->on('base_cars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specifications');
    }
}
