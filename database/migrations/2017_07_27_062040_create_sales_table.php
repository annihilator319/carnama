<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->date('selling_Date');
            $table->integer('selling_Price');
            $table->integer('showroom_Commission');
            $table->integer('company_Commission');
            $table->integer('customer_id')->unsigned();
            $table->integer('newCar_id')->unsigned()->nullable();
            $table->integer('usedCar_id')->unsigned()->nullable();
            $table->foreign('customer_id')->references('id')->on('users');
            $table->foreign('newCar_id')->references('id')->on('new_cars');
            $table->foreign('usedCar_id')->references('id')->on('used_cars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
