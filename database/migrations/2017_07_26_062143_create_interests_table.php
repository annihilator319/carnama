<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('showroom_id')->unsigned();
            $table->integer('newCar_id')->unsigned()->nullable();
            $table->integer('usedCar_id')->unsigned()->nullable();
            $table->foreign('showroom_id')->references('id')->on('showrooms');
            $table->foreign('newCar_id')->references('id')->on('new_cars');
            $table->foreign('usedCar_id')->references('id')->on('used_cars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests');
    }
}
