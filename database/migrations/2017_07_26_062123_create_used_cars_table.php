<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsedCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('used_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_city');
            $table->string('color');
            $table->integer('ODO_reading');
            $table->integer('demand');
            $table->string('remarks');
            $table->boolean('approved');
            $table->boolean('sold');
            $table->integer('previous_owners');
            $table->integer('baseCar_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('baseCar_id')->references('id')->on('base_cars');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('used_cars');
    }
}
