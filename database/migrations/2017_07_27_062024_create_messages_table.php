<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('read');
            $table->boolean('closed');
            $table->boolean('talk_on_phone');
            $table->integer('usedCar_id')->nullable()->unsigned()->default(NULL);
            $table->integer('newCar_id')->unsigned()->nullable()->default(NULL);
            $table->string('message')->nullable()->default(NULL);
            $table->integer('sender_id')->unsigned();
            $table->integer('parent_message')->unsigned()->nullable()->default(NULL);
            $table->integer('handler_id')->unsigned()->nullable()->default(NULL);
            $table->foreign('sender_id')->references('id')->on('users');
            $table->foreign('usedCar_id')->references('id')->on('used_cars');
            $table->foreign('newCar_id')->references('id')->on('new_cars');
            $table->foreign('handler_id')->references('id')->on('users');
            $table->foreign('parent_message')->references('id')->on('messages');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
