<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ODO_reading');
            $table->integer('demand');
            $table->integer('engine_number');
            $table->integer('chasis_number');
            $table->string('color');
            $table->string('importYear');
            $table->integer('baseCar_id')->unsigned();
            $table->integer('showroom_id')->unsigned();
            $table->foreign('baseCar_id')->references('id')->on('base_cars');
            $table->foreign('showroom_id')->references('id')->on('showrooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_cars');
    }
}
