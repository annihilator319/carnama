<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    protected $connection = 'mysql';

    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'address'
    ];

    protected $hidden = [
         'remember_token',
    ];


}
