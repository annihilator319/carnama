<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseCar extends Model
{
    public function Specifications()
    {
        return $this->hasOne('App\Specifications', 'baseCar_id');
    }
}
