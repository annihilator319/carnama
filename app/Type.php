<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function User()
    {
        return $this->hasOne('App\User', 'type_id');
    }
}
