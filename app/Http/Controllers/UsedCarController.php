<?php

namespace App\Http\Controllers;

use App\UsedCar;
use Illuminate\Http\Request;

class UsedCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsedCar  $usedCar
     * @return \Illuminate\Http\Response
     */
    public function show(UsedCar $usedCar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsedCar  $usedCar
     * @return \Illuminate\Http\Response
     */
    public function edit(UsedCar $usedCar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsedCar  $usedCar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsedCar $usedCar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsedCar  $usedCar
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsedCar $usedCar)
    {
        //
    }
}
