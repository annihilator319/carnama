<?php

namespace App\Http\Controllers;

use App\BaseCar;
use App\NewCar;
use App\Showroom;
use Illuminate\Http\Request;
use function response;
use Illuminate\Support\Facades\Auth;

class NewCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('admin.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $showrooms = Showroom::select('id','name')->get();
        $baseCar = BaseCar::all();
        return response()->view('admin.newcar.form', array('showrooms' => $showrooms, 'basecars' => $baseCar));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check())
        {
            if(Auth::user()->type_id == 3)
            {
                $this->validate($request, [
                    'engine_number' => 'required|integer',
                    'ODO' => 'required|integer',
                    'chasis_number' => 'required|integer',
                    'color' => 'required|string',
                    'demand' => 'required|integer|min:10000',
                    'importYear' => 'required|integer|min:1900',
                    'showroom' => 'required|string',
                    'basecar' => 'required|string',
                ]);

                $newCar = new NewCar();
                $newCar->engine_number = $request->engine_number;
                $newCar->ODO = $request->ODO;
                $newCar->color = $request->color;
                $newCar->chasis_number = $request->chasis_number;
                $newCar->demand = $request->demand;
                $newCar->importYear = $request->importYear;
                $newCar->baseCar_id = $request->basecar;
                $newCar->showroom_id = $request->showroom;

                $newCar->save();
                return response()->view('admin.newcar.form');
            }
            else
                echo "Only admin can add a car";
        }
        else
            echo "User not logged in";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewCar  $newCar
     * @return \Illuminate\Http\Response
     */
    public function show(NewCar $newCar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewCar  $newCar
     * @return \Illuminate\Http\Response
     */
    public function edit(NewCar $newCar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewCar  $newCar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewCar $newCar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewCar  $newCar
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewCar $newCar)
    {
        //
    }
}
