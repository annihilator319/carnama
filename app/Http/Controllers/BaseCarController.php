<?php

namespace App\Http\Controllers;

use App\BaseCar;
use App\Specifications;
use App\User;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use function print_r;
use function response;
use function view;

class BaseCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->view('admin\home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('admin.basecar.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            if ($user->type_id == 3) //could't get User->Type to work
            {

                $this->validate($request, [
                    'name' => 'required|string',
                    'manufacturer' => 'required|string',
                    'variant' => 'required|string',
                    'newPrice' => 'required|integer|min:10000',
                    'year_of_Manufacturer' => 'required|integer|min:1900',
                    'bodyType' => 'required|string',
                    'seats' => 'required|integer|min:2',
                    'engineSize' => 'required|integer',
                    'CC' => 'required|integer',
                    'torque' => 'required|integer',
                    'hp' => 'required|integer',
                    'gearBox' => 'required|string',
                    'MultiMedia' => 'required|string',

                ]);

                $specs = new Specifications();
                $specs->engine_size = $request->engine_size;
                $specs->CC = $request->CC;
                $specs->torque = $request->torque;
                $specs->horse_Power = $request->hp;
                $specs->AirBags = $request->AirBags;
                $specs->seats = $request->seats;
                $specs->year_of_Manufacturer = $request->year_of_Manufacturer;
                $specs->gearBox = $request->gearBox;
                $specs->bodyType = $request->bodyType;
                $specs->MultiMedia = $request->MultiMedia;
                $specs->power_steering = $request->power_steering;
                $specs->power_windows = $request->power_windows;
                $specs->ABS = $request->ABS;
                $specs->SunRoof = $request->SunRoof;

                $baseCar = new BaseCar();
                $baseCar->manufacturer = $request->manufacturer;
                $baseCar->name = $request->name;
                $baseCar->variant = $request->variant;
                $baseCar->newPrice = $request->newPrice;

                $baseCar->save();
                $baseCar->Specifications()->save($specs);
                return response()->view('admin.basecar.form');


            } else {
                echo "Only admin can perform this action";
            }
        }
        else
            echo "Please Log-in";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BaseCar $baseCar
     * @return \Illuminate\Http\Response
     */
    public function show(BaseCar $baseCar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BaseCar $baseCar
     * @return \Illuminate\Http\Response
     */
    public function edit(BaseCar $baseCar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\BaseCar $baseCar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BaseCar $baseCar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BaseCar $baseCar
     * @return \Illuminate\Http\Response
     */
    public function destroy(BaseCar $baseCar)
    {
        //
    }
}
