<?php
/**
 * Created by PhpStorm.
 * User: Hamza Zafer
 * Date: 7/29/2017
 * Time: 6:04 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Messages')

@section('content_header')
@stop

@section('content')
    <div class="row" >
        <div class="col-md-5" >  <!--left side -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-justified">
                        <li class="bg-gray-active"><a href="#"><i class="fa fa-inbox"></i>New Cars</a></li>
                        <li><a href="#"><i class="fa fa-envelope-o"></i>Used Cars</a></li>
                        <li><a href="#"><i class="fa fa-file-text-o"></i>Approvals</a></li>
                    </ul>
                </div>
            </div>
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Inbox</h3>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm" placeholder="Search Messages">
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                </div>

                <div class="box-body no-padding">
                    <div class="mailbox-controls">

                        <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>

                        <div class="pull-right">
                            10 per page
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped">
                            <tbody class="text-center">
                            <tr>
                                <td>Name</td>
                                <td>Subject</td>
                                <td>Time</td>
                            </tr>
                            <tr>
                                <td class="mailbox-name"><b>Hamza Zafar</b></td>
                                <td class="mailbox-subject">This is sample text</td>
                                <td class="mailbox-date">5 mins ago</td>
                            </tr>
                            <tr>
                                <td class="mailbox-name"><b>Hamza Zafar</b></td>
                                <td class="mailbox-subject">This is sample text</td>
                                <td class="mailbox-date">20 mins ago</td>
                            </tr>
                            <tr>
                                <td class="mailbox-name"><b>Hamza Zafar</b></td>
                                <td class="mailbox-subject">This is sample text</td>
                                <td class="mailbox-date">20 mins ago</td>
                            </tr>
                            <tr>
                                <td class="mailbox-name"><b>Hamza Zafar</b></td>
                                <td class="mailbox-subject">This is sample text</td>
                                <td class="mailbox-date">20 mins ago</td>
                            </tr>
                            <tr>
                                <td class="mailbox-name"><b>Hamza Zafar</b></td>
                                <td class="mailbox-subject">This is sample text</td>
                                <td class="mailbox-date">20 mins ago</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-7">  <!--Right side -->
            <div class="box box-default direct-chat direct-chat-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Conversation</h3>
                </div>
                <div class="box-body">
                    <div class="direct-chat-messages">
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">User</span>
                                <span class="direct-chat-timestamp pull-right">29 Jly 2:00 pm</span>
                            </div>
                            <div class="direct-chat-text">
                                Sample message.
                            </div>
                        </div>
                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">User</span>
                                <span class="direct-chat-timestamp pull-right">29 Jly 2:00 pm</span>
                            </div>
                            <div class="direct-chat-text">
                                Sample message.
                            </div>
                        </div>

                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">Hamza Zafar</span>
                                <span class="direct-chat-timestamp pull-left">29 Jly 2:05 pm</span>
                            </div>
                            <div class="direct-chat-text">
                                Helloooooooooooooooooooooooooooooooooooooooooooo.
                            </div>
                        </div>
                        <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-right">Hamza Zafar</span>
                                <span class="direct-chat-timestamp pull-left">29 Jly 2:05 pm</span>
                            </div>
                            <div class="direct-chat-text">
                                Helloooooooooooooooooooooooooooooooooooooooooooo.
                            </div>
                        </div>

                        <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left">User</span>
                                <span class="direct-chat-timestamp pull-right">29 Jly 2:00 pm</span>
                            </div>
                            <div class="direct-chat-text">
                                Sample message.
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box-footer">
                    <div class="input-group">
                        <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                        <span class="input-group-btn">
                <button type="button" class="btn btn-default">Send</button>
                </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')

@stop

@section('js')
@stop