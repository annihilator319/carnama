<?php
/**
 * Created by PhpStorm.
 * User: Hamza Zafer
 * Date: 7/29/2017
 * Time: 2:35 PM
 */
?>
@extends('adminlte::page')

@section('title', 'Add New Car')

@section('js')

@stop

@section('css')

@stop

@section('content_header')

@stop

@section('content')
    <div class="panel panel-default ">
        <div class="panel-heading">Add New Car</div>
        <div class="panel-body  text-center">
            <form method="POST" action="{{ route('NewCar.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group{{ $errors->has('ODO') ? ' has-error' : '' }}">
                                <label for="ODO" class="col-md-4 control-label">ODO Meter Reading</label>

                                <div class="col-md-6">
                                    <input id="ODO" type="number" class="form-control" name="ODO"
                                           value="{{ old('ODO') }}"
                                           required
                                           autofocus>

                                    @if ($errors->has('ODO'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ODO') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('engine_number') ? ' has-error' : '' }}">
                                <label for="engine_number" class="col-md-4 control-label">Engine Number</label>

                                <div class="col-md-6">
                                    <input id="engine_number" type="number" class="form-control" name="engine_number"
                                           value="{{ old('engine_number') }}" required autofocus>

                                    @if ($errors->has('engine_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('engine_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('chasis_number') ? ' has-error' : '' }}">
                                <label for="chasis_number" class="col-md-4 control-label">Chassis Number</label>

                                <div class="col-md-6">
                                    <input id="chasis_number" type="number" class="form-control" name="chasis_number"
                                           value="{{ old('chasis_number') }}"
                                           required autofocus>

                                    @if ($errors->has('chasis_number'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('chasis_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('basecar') ? ' has-error' : '' }}">
                                <label for="basecar" class="col-md-4 control-label">Base Car</label>

                                <div class="col-md-6">
                                    <select id="basecar" class="form-control" name="basecar"  value="{{ old('basecar') }}" required>
                                        <option @default hidden selected value="">Please pick one</option>
                                        @foreach($basecars as $car)
                                            <option value="{{$car->id}}">{{$car->manufacturer}} {{$car->name}} {{$car->variant}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('showroom'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('showroom') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>

                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group{{ $errors->has('demand') ? ' has-error' : '' }}">
                                <label for="demand" class="col-md-4 control-label">Demand</label>

                                <div class="col-md-6">
                                    <input id="demand" type="number" class="form-control" name="demand"
                                           value="{{ old('demand') }}" required>

                                    @if ($errors->has('demand'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('demand') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                                <label for="color" class="col-md-4 control-label">Color</label>

                                <div class="col-md-6">
                                    <input id="color" type="text" class="form-control" name="color"
                                           value="{{ old('color') }}" required>

                                    @if ($errors->has('color'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group{{ $errors->has('importYear') ? ' has-error' : '' }}">
                                <label for="importYear" class="col-md-4 control-label">Import Year</label>

                                <div class="col-md-6">
                                    <input id="importYear" type="number" class="form-control" name="importYear"
                                           value="{{ old('importYear') }}" required>

                                    @if ($errors->has('importYear'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('importYear') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group{{ $errors->has('showroom') ? ' has-error' : '' }}">
                                <label for="showroom" class="col-md-4 control-label">Showroom</label>

                                <div class="col-md-6">
                                    <select id="showroom" class="form-control" name="showroom"  value="{{ old('showroom') }}" required>
                                        <option @default hidden selected value="">Please pick one</option>
                                        @foreach($showrooms as $showroom)
                                            <option value="{{$showroom->id}}">{{$showroom->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('showroom'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('showroom') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <br/>

                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-default">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
