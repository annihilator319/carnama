<?php
/**
 * Created by PhpStorm.
 * User: Hamza Zafer
 * Date: 7/29/2017
 * Time: 12:24 AM
 */
?>

@extends('adminlte::page')

@section('title', 'Add new base car')

@section('js')

@stop

@section('css')

@stop

@section('content_header')

@stop

@section('content')
    <div class="panel panel-default ">
        <div class="panel-heading">New Base Car</div>
        <div class="panel-body  text-center">
            <form method="POST" action="{{ route('BaseCar.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ old('name') }}"
                                           required
                                           autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('manufacturer') ? ' has-error' : '' }}">
                                <label for="manufacturer" class="col-md-4 control-label">Manufacturer</label>

                                <div class="col-md-6">
                                    <input id="manufacturer" type="text" class="form-control" name="manufacturer"
                                           value="{{ old('manufacturer') }}" required autofocus>

                                    @if ($errors->has('manufacturer'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('manufacturer') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('variant') ? ' has-error' : '' }}">
                                <label for="variant" class="col-md-4 control-label">Variant</label>

                                <div class="col-md-6">
                                    <input id="variant" type="text" class="form-control" name="variant"
                                           value="{{ old('variant') }}"
                                           required autofocus>

                                    @if ($errors->has('variant'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('variant') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('newPrice') ? ' has-error' : '' }}">
                                <label for="newPrice" class="col-md-4 control-label">New Price</label>

                                <div class="col-md-6">
                                    <input id="newPrice" type="number" class="form-control" name="newPrice"
                                           value="{{ old('newPrice') }}" required>

                                    @if ($errors->has('newPrice'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('newPrice') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('year_of_Manufacturer') ? ' has-error' : '' }}">
                                <label for="year_of_Manufacturer" class="col-md-4 control-label">Year of
                                    Manufacture</label>

                                <div class="col-md-6">
                                    <input id="year_of_Manufacturer" type="number" class="form-control"
                                           name="year_of_Manufacturer"
                                           value="{{ old('year_of_Manufacturer') }}" required>

                                    @if ($errors->has('year_of_Manufacturer'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('year_of_Manufacturer') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('bodyType') ? ' has-error' : '' }}">
                                <label for="bodyType" class="col-md-4 control-label">Body Type</label>

                                <div class="col-md-6">
                                    <input id="bodyType" type="text" class="form-control"
                                           name="bodyType"
                                           value="{{ old('bodyType') }}" required>

                                    @if ($errors->has('bodyType'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('bodyType') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('seats') ? ' has-error' : '' }}">
                                <label for="seats" class="col-md-4 control-label">Seats</label>

                                <div class="col-md-6">
                                    <input id="seats" type="number" class="form-control"
                                           name="seats"
                                           value="{{ old('seats') }}" required>

                                    @if ($errors->has('seats'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('seats') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>

                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group{{ $errors->has('engineSize') ? ' has-error' : '' }}">
                                <label for="engineSize" class="col-md-4 control-label">Engine Size</label>

                                <div class="col-md-6">
                                    <input id="engineSize" type="number" class="form-control" name="engineSize"
                                           value="{{ old('engineSize') }}" required>

                                    @if ($errors->has('engineSize'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('engineSize') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group{{ $errors->has('CC') ? ' has-error' : '' }}">
                                <label for="CC" class="col-md-4 control-label">CC</label>

                                <div class="col-md-6">
                                    <input id="CC" type="number" class="form-control" name="CC"
                                           value="{{ old('CC') }}" required>

                                    @if ($errors->has('CC'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('CC') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group{{ $errors->has('torque') ? ' has-error' : '' }}">
                                <label for="torque" class="col-md-4 control-label">Torque</label>

                                <div class="col-md-6">
                                    <input id="torque" type="number" class="form-control" name="torque"
                                           value="{{ old('torque') }}" required>

                                    @if ($errors->has('torque'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('torque') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group{{ $errors->has('hp') ? ' has-error' : '' }}">
                                <label for="hp" class="col-md-4 control-label">HorsePower</label>

                                <div class="col-md-6">
                                    <input id="hp" type="number" class="form-control" name="hp"
                                           value="{{ old('hp') }}" required>

                                    @if ($errors->has('hp'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('hp') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('gearBox') ? ' has-error' : '' }}">
                                <label for="gearBox" class="col-md-4 control-label">GearBox</label>

                                <div class="col-md-6">
                                    <input id="gearBox" type="text" class="form-control" name="gearBox"
                                           value="{{ old('gearBox') }}" required>

                                    @if ($errors->has('gearBox'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gearBox') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('MultiMedia') ? ' has-error' : '' }}">
                                <label for="MultiMedia" class="col-md-4 control-label">MultiMedia</label>

                                <div class="col-md-6">
                                    <input id="MultiMedia" type="text" class="form-control" name="MultiMedia"
                                           value="{{ old('MultiMedia') }}" required>

                                    @if ($errors->has('MultiMedia'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('MultiMedia') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <label for="AirBags" class="col-xs-2">Air Bag
                        <label class="radio-inline">
                            <input type="radio" name="AirBags" id="AirBags" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="AirBags" id="AirBags" value="0" checked> No
                        </label>
                    </label>

                    <label for="power_steering" class="col-xs-3">Power Steering
                        <label class="radio-inline">
                            <input type="radio" name="power_steering" id="power_steering" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="power_steering" id="power_steering" value="0" checked> No
                        </label>
                    </label>

                    <label for="power_windows" class="col-xs-3">Power Windows
                        <label class="radio-inline">
                            <input type="radio" name="power_windows" id="power_windows" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="power_windows" id="power_windows" value="0" checked> No
                        </label>
                    </label>

                    <label for="ABS" class="col-xs-2">ABS
                        <label class="radio-inline">
                            <input type="radio" name="ABS" id="ABS" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="ABS" id="ABS" value="0" checked> No
                        </label>
                    </label>

                    <label for="SunRoof" class="col-xs-2">Sun Roof
                        <label class="radio-inline">
                            <input type="radio" name="SunRoof" id="SunRoof" value="1"> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="SunRoof" id="SunRoof" value="0" checked> No
                        </label>
                    </label>
                </div>

                <br/>

                <div class="form-group">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-default">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
